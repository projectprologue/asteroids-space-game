package  {
	/**
	 * ...
	 * @author Boy Voesten
	 */
	public class Enemy01 extends AI {
		
		public function Enemy01() {
			super();
			asset = new EnemyEasy();
			hp = 1;
			enemyScore = 10;
		}
	}
}
