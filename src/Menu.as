package {
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import flash.system.fscommand;
	
	/**
	 * @author Nick van Dokkum
	 */
	public class Menu extends Sprite 
	{
		private var _game:Game;
		private var myStartButton:StartButton;
		private var myQuitButton:QuitButton;
		private var buttonsHolder:MovieClip = new MovieClip();
		private var _background:MenuBG;
		private var _menuMusic:Sound = new StartmenuMusic();
		private var _soundchannelMenu:SoundChannel;
		private var _buttonClick:Sound = new MouseClick();
		private var _soundchannelClick:SoundChannel;
		
		public function Menu():void
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			_soundchannelMenu = _menuMusic.play(0,9999999999999);
			
			_background = new MenuBG;
			_background.width = stage.stageWidth;
			_background.height = stage.stageHeight;
			addChild(_background);
			
			
			myStartButton = new StartButton();
			myStartButton.x = 220;
			myStartButton.y = 280; 
			
			myQuitButton = new QuitButton();
			myQuitButton.x = 245;
			myQuitButton.y = 384;
			
			addChild(buttonsHolder);
			buttonsHolder.addChild(myStartButton);
			buttonsHolder.addChild(myQuitButton);
			buttonsHolder.addEventListener(MouseEvent.CLICK, onClick);
		}
		
		
		private function onClick(e:MouseEvent):void 
		{
			if (e.target == myStartButton)
			{
				_soundchannelClick = _buttonClick.play();
				destroy();
				dispatchEvent(new Event("startGame"));
			}
			if (e.target == myQuitButton)
			{
				_soundchannelClick = _buttonClick.play();
				fscommand("quit");
			}
		}
		private function destroy():void 
		{
			buttonsHolder.removeEventListener(MouseEvent.CLICK, onClick);
			removeChild(buttonsHolder);
			removeChild(_background);
			_soundchannelMenu.stop();
		}
	}
}
