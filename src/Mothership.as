package {
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	/**
	 * ...
	 * @author Boy Voesten
	 */
	public class Mothership extends Sprite {
		
		private var _mothershipArt : mothershipArt;
		private var _player	: Player;
		private var _explosionDelay : Timer;
		private var _alphaFlicker : Timer;
		public var hp : int = 3;
		
		public function Mothership() {
			super();
			
			this.addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// Entry
			
			_mothershipArt = new mothershipArt;
			_mothershipArt.gotoAndStop(1);
			addChild(_mothershipArt);
			
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		private function update(e:Event):void {
			if (hp == 0) {
				removeEventListener(Event.ENTER_FRAME, update);
				
				_mothershipArt.gotoAndStop(2);
				dispatchEvent(new Event("playerExplode")); 
				// Wait a bit for the animation to finish
				_explosionDelay = new Timer(450, 1);
				_explosionDelay.addEventListener(TimerEvent.TIMER, function(e:Event):void {
					trace("shout gameover");
					dispatchEvent(new Event("gameover")); 
				});
				_explosionDelay.start();
			}
		}
		
		public function hit():void {
			hp -= 1;
			_mothershipArt.alpha -= .6;
			_alphaFlicker = new Timer(150, 1);
			_alphaFlicker.addEventListener(TimerEvent.TIMER, function(e:Event):void {
				_mothershipArt.alpha += .6;
			});
			_alphaFlicker.start();
			trace("HP = " + hp);
		}
	}
}
