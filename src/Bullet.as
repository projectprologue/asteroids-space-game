package  {
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Boy Voesten
	 */
	public class Bullet extends Sprite {
		
		protected var _asset	:	MovieClip;
		protected var _speed	:	Number;
		protected var _stepX	:	Number;
		protected var _stepY	:	Number;
		private var isOutOfBounds : Boolean = false;
		
		public function Bullet() {
			super();
			
			_speed			=	10;
			_asset			=	new BulletArt();
			_asset.scaleX	=	0.3;
			_asset.scaleY	=	_asset.scaleX;
			addChild(_asset);
		}
		
		public function setDirection(angle : Number ) : void {
			// This sets the direction the bullet has to fly to
			// From degrees to radian:
			var radian	:	Number	=	angle / (180 / Math.PI);
			_stepX	=	Math.cos( radian ) * _speed;
			_stepY	=	Math.sin( radian ) * _speed;
			
			// This rotates the bullet into the right direction
			var diffX				:	Number	=	_stepX - this.x;
			var diffY				:	Number	=	_stepY - this.y;
			var rotationInRadians	:	Number	=	Math.atan2(diffY, diffX);
			_asset.rotation						=	rotationInRadians * (180 / Math.PI);
		}
		
		public function update() : void {
			// Executes the calculated direction from the setDirection function
			this.x	+=	_stepX;
			this.y	+=	_stepY;
			
			// Check if the bullet is out of bounds
			if (this.x > stage.stageWidth || this.x < -10 || this.y > stage.stageHeight || this.y < -10) {
				isOutOfBounds = true;
			}
		}
		
		public function GetOutOfBounds():Boolean {
			return isOutOfBounds;
		}

	}
	
}
