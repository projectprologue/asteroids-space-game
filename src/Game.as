package  {
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.Timer;
	import flash.geom.Vector3D;
	import flash.media.Sound;
	import flash.media.SoundChannel;

	
	/**
	 * ...
	 * @author Boy Voesten
	 */
	public class Game extends Sprite {
		
		
		// Core
		private var _player		:	Player;
		private var _bullets	:	Array;
		private var _background	:	Background;
		private var _mothership :	Mothership;
		private var _enemies	:	Vector.<AI>;
		// Wave system
		private var _wave				:	int	= 1;
		private var _timeBetweenWaves	:	Timer;
		private var _enemySpawntime		:	Timer;
		private var _enemySpawnLocation	:	Vector3D;
		private var _enemySpawnSide		:	int;
		private var _spawnsPerWave		:	int;
		private var _spawnSpeed			:	Number	= 2050;
		// Sounds
		private var _BgMusic			:	Sound	= new BgMusic();
		private var _fireSound			:	Sound	= new Fire();
		private var _soundchannelBg		:	SoundChannel;
		private var _soundchannelFire	:	SoundChannel;
		// Fire related
		private var _fireDelay	:	Timer;
		private var _canFire	:	Boolean 	= true;
		// UI
		private var _txtHP		:	TextField 	= new TextField();
		private var _txtWave	:	TextField 	= new TextField();
		private var _txtScore	:	TextField 	= new TextField();
		private var _format		:	TextFormat 	= new TextFormat();
		public static var 	score		:	Number 		= 0;
		// HitTests & Effects
		public static var enemyHitTest	:	Boolean = true;
		
		
		public function Game() {
			this.addEventListener(Event.ADDED_TO_STAGE, init);
			trace("games main loaded");
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.ENTER_FRAME, update);
			
			// Play the Game's background music
			_soundchannelBg = _BgMusic.play(0, 9999999999999);
			
			// INSTANTIATING
			_player = new Player;
			_player.x = stage.stageWidth 	/ 2;
			_player.y = stage.stageHeight 	/ 2;
			
			_mothership = new Mothership;
			_mothership.x = stage.stageWidth 	/ 2;
			_mothership.y = stage.stageHeight 	/ 2;
			
			_txtHP.text	= "HP: " + String(_mothership.hp);
			_txtHP.x = 25;
			_txtHP.y = 25;
			
			_txtWave.text = "Wave: " + String(_wave);
			_txtWave.x = 25;
			_txtWave.y = 50;
			
			_txtScore.text = "Score: " + String(score);
			_txtScore.x = 25;
			_txtScore.y = 75;
			
			_format.color = 0xFFFFFF;
			_format.font = "Verdana";
			_format.size = 15;
			_format.bold = true;
			
			_txtHP.setTextFormat( _format );
			_txtHP.autoSize = TextFieldAutoSize.LEFT;
			_txtHP.selectable = false;
			_txtWave.setTextFormat( _format );
			_txtWave.autoSize = TextFieldAutoSize.LEFT;
			_txtWave.selectable = false;
			_txtScore.setTextFormat( _format );
			_txtScore.autoSize = TextFieldAutoSize.LEFT;
			_txtScore.selectable = false;
			
			_background = new Background;
			_background.x = stage.stageWidth / 2;
			_background.y = stage.stageHeight / 2;
			_background.width = stage.stageWidth;
			_background.height = stage.stageHeight;
			
			addChild(_player);
			addChild(_mothership);
			addChild(_txtHP);
			addChild(_txtWave);
			addChild(_txtScore);
			addChildAt(_background, 0);
			
			// Here we listen for the Dispatch Event we created to yell SHOOT So we can then execute the function fire
			_player.addEventListener(Player.SHOOT, fire);
			_mothership.addEventListener("gameover", gameover);
			_mothership.addEventListener("playerExplode", function(e:Event):void {
				_player.explode();
			});
			
			// Array for the bullets
			_bullets = [];
			
			// To spawn the enemyd
			_enemies = new Vector.<AI>;
			_timeBetweenWaves = new Timer(4000, 2);
			_timeBetweenWaves.addEventListener(TimerEvent.TIMER, nextWave);	
			_timeBetweenWaves.start();
			
			// Setup the fireTimer and attach a listener to it.
			_fireDelay = new Timer(200, 1);
			_fireDelay.addEventListener(TimerEvent.TIMER, fireTimerHandler, false, 0, true);
		}
		
		private function gameover(e:Event):void {
			trace("shout received");
			
			// Remove all the eventlisteners
			removeEventListener(Event.ENTER_FRAME, update);
			_player.destroy();
			_enemySpawntime.removeEventListener(TimerEvent.TIMER, nextSpawn);
			_mothership.removeEventListener(Event.ENTER_FRAME, update);
			_mothership.removeEventListener("gameover", gameover);
			if (_timeBetweenWaves) _timeBetweenWaves.removeEventListener(TimerEvent.TIMER, nextWave);
			if (_fireDelay) _fireDelay.removeEventListener(TimerEvent.TIMER, fireTimerHandler);
			_soundchannelBg.stop();
			
			// Tell the main to open the menu
			dispatchEvent(new Event("openMenu"));
		}
		
		private function nextWave(e:TimerEvent):void {
			if (_timeBetweenWaves.currentCount > 1) {
				// To spawn the enemy
				if (_spawnSpeed >= 500) _spawnSpeed -= 50;
				_spawnsPerWave = 2 * _wave;
				_enemySpawntime = new Timer(_spawnSpeed, _spawnsPerWave);
				_enemySpawntime.addEventListener(TimerEvent.TIMER, nextSpawn);
				_enemySpawntime.start();
				_timeBetweenWaves.reset();
				trace("WAVE: " + _wave);
				trace("Spawntime is: " + _spawnSpeed);
				// Update UI
				_txtWave.text = "Wave: " + String(_wave);
				_txtWave.setTextFormat( _format );
			}
		}
		
		private function nextSpawn(e:TimerEvent):void {
			// This will make it so that the AI spawns from all sides, randomized.
			_enemySpawnSide = Math.random() * 4; 
			if (_enemySpawnSide == 0)_enemySpawnLocation = new Vector3D(Math.random() * stage.stageWidth, 0);
		    if (_enemySpawnSide == 1)_enemySpawnLocation = new Vector3D(0								, Math.random() * stage.stageHeight);
		    if (_enemySpawnSide == 2)_enemySpawnLocation = new Vector3D(Math.random() * stage.stageWidth, stage.stageHeight);
		    if (_enemySpawnSide == 3)_enemySpawnLocation = new Vector3D(stage.stageWidth				, Math.random() * stage.stageHeight);

																		// EnemyEasy and EnemyMed
			if (_wave >= 5 && _wave <= 10) {
				if (_enemySpawnSide == 0) {
					_enemies.push(new Enemy02());
				} else {
					_enemies.push(new Enemy01());
				}
				_enemies[_enemies.length - 1].x = _enemySpawnLocation.x;
				_enemies[_enemies.length - 1].y = _enemySpawnLocation.y;
			} else if (_wave >= 10) {									// EnemyEasy, EnemyMed and EnemyHard
				if (_enemySpawnSide == 0) {
					_enemies.push(new Enemy03());
				} else if (_enemySpawnSide == 1) {
					_enemies.push(new Enemy02());
				} else {
					_enemies.push(new Enemy01());
				}
				_enemies[_enemies.length - 1].x = _enemySpawnLocation.x;
				_enemies[_enemies.length - 1].y = _enemySpawnLocation.y;
			} else {													// EnemyEasy only
				_enemies.push(new Enemy01());
				_enemies[_enemies.length - 1].x = _enemySpawnLocation.x;
				_enemies[_enemies.length - 1].y = _enemySpawnLocation.y;
			}
			
			// The wave system
			addChild(_enemies[_enemies.length -1]);
			if (_enemySpawntime.currentCount == _spawnsPerWave) {
				_wave++;
				_timeBetweenWaves.start();
				_enemySpawntime.reset();
			}
		}
		
		private function update(e:Event):void {
			// Update BULLETS
			for (var i : int = _bullets.length - 1; i >= 0; i-- ) {
				_bullets[i].update();
				
				// Check for BULLET on ENEMY collision
				for (var j : int = _enemies.length - 1; j >= 0; j-- ) {
					if (_bullets.length > i && _enemies[j].hitTestPoint(_bullets[i].x, _bullets[i].y, true)) {
						_enemies[j].hit();
						removeChild(_bullets[i]);
						_bullets.splice(i, 1);
						// Update UI
						_txtScore.text = "Score: " + score;
						_txtScore.setTextFormat( _format );
					}
				}
				
				// Check if OutOfBounds or touching MOTHERSHIP
				//if ( _bullets[i].hitTestObject(_mothership)) { 
				if (i > 0 && i < _bullets.length)
				{
					var disBulletOnMother:int = Point.distance(new Point(_bullets[i].x, _bullets[i].y), new Point(_mothership.x, _mothership.y));
					if (_bullets[i].GetOutOfBounds() || _bullets[i].width * 0.4 + _mothership.width * 0.4 > disBulletOnMother) {
						removeChild(_bullets[i]);
						_bullets.splice(i, 1);
					}
				}
			}
			
			// Update ENEMIES
			for (var k : int = _enemies.length - 1; k >= 0; k-- ) {
				_enemies[k].update();
				
				// Check for ENEMY on MOTHERSHIP collision
				//else if (_enemies[k].hitTestObject(this._mothership)) {
				var disEnemyOnMother:int = Point.distance(new Point(_enemies[k].x, _enemies[k].y), new Point(_mothership.x, _mothership.y));
				if (_enemies[k].width / 2 + _mothership.width / 2 > disEnemyOnMother) {
					if (!_enemies[k].enemyHitMothership && _enemies[k].enemyHitTest) {
						trace("- Hit -");
						_enemies[k].explode();
						_enemies[k].enemyHitMothership = true;
						_mothership.hit();
						// Update UI
						_txtHP.text = "HP: " + String(_mothership.hp);
						_txtHP.setTextFormat( _format );
					}
				}
				
				// Checks if it can be removed or not
				if (_enemies[k].GetEnemyRemovable()) {
						score += _enemies[k].score;
						_txtScore.text = "Score: " + score;
						_txtScore.setTextFormat( _format );
						removeChild(_enemies[k]);
						_enemies.splice(k, 1);
						trace("Enemy spliced");
				}
				
				
			}
		}
		
		// This function will run every time the Event Listener hears 'SHOOT' from the turret
		private function fire(e:Event):void {
			if (_canFire) {
				// Play shooting sound
				_soundchannelFire = _fireSound.play();
				
				// The Game Class will create a new bullet and send it to the mouse it's location
				var newBullet	:	Bullet	=	new Bullet();
				newBullet.setDirection( _player.shootDirection );
				newBullet.x		=	_player.x;
				newBullet.y		=	_player.y;
				
				var _rad:Number = _player.rotation * Math.PI / 180;
				var _speedX:Number = 1;
				var _speedY:Number = 1;
				_bullets.x = _player.x + 10 * Math.cos(_rad);
				_bullets.y = _player.y + 10 * Math.sin(_rad);
				
				addChildAt(newBullet, 1);
				
				// We push the bullet into the array which updates every single frame in the update function
				_bullets.push(newBullet);
				
				// Reset the delay process
				_canFire = false;
				_fireDelay.start();
			}
		}
 
 		private function fireTimerHandler(e:TimerEvent):void {
			// Delay has passed, you can fire again
			_canFire = true;
		}
	}
}
