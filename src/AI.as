package  {
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.geom.Vector3D;
	import flash.utils.Timer;
	/**
	 * ...
	 * @author Boy Voesten
	 */
	public class AI extends Sprite {
		
		// Core
		protected var asset : MovieClip;
		private var _game 	: Game;
		private var _aiSpawn	: Vector3D;
		private var _origin		: Vector3D;
		private var _velocity	: Vector3D;
		private var _speed		: int = 2;
		// UI
		public var score : Number = 0;
		public var hp : int = 3;
		protected var enemyScore : int;
		// HitTests & Effects
		private var _enemyRemovable	: Boolean = false;
		private var _explosionDelay : Timer;
		private var _alphaFlicker 	: Timer;
		public var enemyHitMothership : Boolean = false;
		public var enemyHitTest : Boolean = true;
		
		
		public function AI() {
			super();
			
			this.addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// Entry
			
			asset.gotoAndStop(1);
			addChild(asset);
			
			_aiSpawn = new Vector3D(this.x, this.y);
			_origin = new Vector3D(stage.stageWidth / 2, stage.stageHeight / 2);
			
			var rad : Number = Math.atan2(this.y - stage.stageHeight / 2, this.x - stage.stageWidth / 2);
			this.rotation = rad * (180 / Math.PI);
			
			_velocity = _origin.subtract(_aiSpawn);
			_velocity.normalize();
			_velocity.scaleBy(_speed);
		}
		
		public function update():void {
			// Make a boolean that goes on true when you hit the mothership.
			if (!_enemyRemovable) {
				this.x += _velocity.x;
				this.y += _velocity.y;
			}
			
			if (hp <= 0) {
				if (!enemyHitTest) return;
				trace("Score1: " + score);
				score += enemyScore;
				trace(enemyScore);
				trace("Score2: " + score);
				explode();
				enemyHitTest = false;
			}
		}
		
		public function explode():void {
			// Check if already started exploding
			if (_explosionDelay && _explosionDelay.running) return;
			
			// Explosion
			trace("Explode animation");
			asset.gotoAndStop(2);
			
			// Wait a bit for the animation to finish
			enemyHitTest = false;
			_explosionDelay = new Timer(300, 1);
			_explosionDelay.addEventListener(TimerEvent.TIMER, function():void {
				_enemyRemovable = true;
				trace("enemyRemovable = true");
				
			});
			_explosionDelay.start();
		}

		public function GetEnemyRemovable():Boolean {
			return _enemyRemovable;
		}
		
		public function hit():void {
			hp -= 1;
			asset.alpha -= .6;
			_alphaFlicker = new Timer(50, 1);
			_alphaFlicker.addEventListener(TimerEvent.TIMER, function(e:Event):void {
				asset.alpha += .6;
			});
			_alphaFlicker.start();
			trace("AI HP = " + hp);
		}
	}
}