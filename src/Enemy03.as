package  {
	/**
	 * ...
	 * @author Boy Voesten
	 */
	public class Enemy03 extends AI {
		
		public function Enemy03() {
			super();
			asset = new EnemyHard();
			hp = 3;
			enemyScore = 30;
		}
	}
}