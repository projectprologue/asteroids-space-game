package  {
	import adobe.utils.CustomActions;
	import calculation.Calculate;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	
	/**
	 * ...
	 * @author Boy Voesten
	 */
	public class Player extends Sprite {
		
		// This variable will be used for our Dispatch Event
		public static const SHOOT : String = "shoot";
		
		private var _player	: playershipArt;
		private var offSet	: Point =  new Point();
		private var _shotTimeCounter :	int = 0;
		private var _rotation :	Number = 0;
		private var _rotationSpeed:Number = 0;
		private var maxspeed:Number = 6;
		private var _acceleration:Number = .2;
		
		public function Player() {
			if(stage) init(null);
            else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, keyPressed);
			stage.addEventListener(KeyboardEvent.KEY_UP, keyReleased);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			addEventListener(Event.ENTER_FRAME, update);
			
			// Add the player to the stage
			_player = new playershipArt();
			addChild(_player);
			
			offSet = Calculate.calculateOffset(this.rotation);
		}
		
		private function mouseDown(e:Event):void {
			if(stage)stage.addEventListener(MouseEvent.MOUSE_UP,mouseUp); 	//listen for mouse up on the stage, in case the finger/mouse moved off of the button accidentally when they release.
			addEventListener(Event.ENTER_FRAME,tick); 				//while the mouse is down, run the tick function once every frame as per the project frame rate
		}

		private function mouseUp(e:Event):void {
			removeEventListener(Event.ENTER_FRAME,tick);  			//stop running the tick function every frame now that the mouse is up
			if (stage)stage.removeEventListener(MouseEvent.MOUSE_UP,mouseUp); //remove the listener for mouse up
		}

		private function tick(e:Event):void {
			dispatchEvent(new Event(SHOOT));
		}
		
		private function keyPressed(e:KeyboardEvent):void {
			if (e.keyCode == Keyboard.LEFT || e.keyCode == Keyboard.A) {
				_rotation = 1;
			}
			else if (e.keyCode == Keyboard.RIGHT || e.keyCode == Keyboard.D) {
				_rotation = -1;
			} else {
				_rotation = 0;
			}
			
			offSet = Calculate.calculateOffset(this.rotation);
		}
		
		private function keyReleased(e:KeyboardEvent):void {
			if (e.keyCode == Keyboard.LEFT || e.keyCode == Keyboard.A || e.keyCode == Keyboard.RIGHT || e.keyCode == Keyboard.D) {
				_rotation = 0;
			}
		}
		
		public function update(e:Event):void {
			
			movement();
			
			// POINT AT MOUSE & Calculate the differense between the player and the mouse
			var p:Point = new Point(mouseX, mouseY);
			p = localToGlobal(p);
			var diffX	:	Number	=	p.x - this.x;
			var diffY	:	Number	=	p.y - this.y;
			
			// Calculate the corner with atan (TOA)
			var rotationInRadians	:	Number	=	Math.atan2(diffY, diffX);
			_player.rotation	=	rotationInRadians * (180 / Math.PI) - this.rotation;
		}

		private function movement():void {
			// CIRCLE OF MOVEMENT
			offSet = Calculate.calculateOffset(this.rotation);
			this.x = offSet.x + stage.stageWidth / 2;
			this.y = offSet.y + stage.stageHeight / 2;
			
			this.rotation -= _rotationSpeed;
				
			if (_rotation == 1 && _rotationSpeed < maxspeed) {
				_rotationSpeed += 1;			// if right is pressed and speed didnt hit the limit, increase speed.
			}
			if (_rotation == -1 && _rotationSpeed > -maxspeed) {
				_rotationSpeed -= 1;			// if left is pressed and speed didnt hit the limit, increase speed (the other way).
			}
			if (_rotationSpeed > 0) {
				_rotationSpeed -= _acceleration;		// if speed is more than 0, decrease.
			}
			if (_rotationSpeed < 0) {
				_rotationSpeed += _acceleration;		// if speed is less than 0, increase.
			}
		}
		
		public function get shootDirection() : Number {
			return _player.rotation + rotation;
		}
		
		public function explode():void {
			_player.gotoAndStop(2);
		}
		
		public function destroy():void {
			removeEventListener(Event.ENTER_FRAME, update);
			removeEventListener(KeyboardEvent.KEY_DOWN, keyPressed);
			removeEventListener(KeyboardEvent.KEY_UP, keyReleased);
		}
	}
}
